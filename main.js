function formSubmit() {
    const messageInput = document.getElementById('message-input');
    const newMessage = messageInput.value;

    if (newMessage) {
        addMessage(newMessage);
        messageInput.value = ''; // reseta o input
    }
    
    return false;
}

function addMessage(message) {
    const messageList = document.getElementById('messages-list');
    messageList.innerHTML += `
    <div class="message-wrapper">
        <div class="message-item message-user">
            <div class="message-sender"><strong>Você diz:</strong></div>
            <div class="message-chat">${message}</div>
        </div>
    </div>
    `;

    messageList.scrollTop = messageList.scrollHeight; // define o scroll para o fim da div
}