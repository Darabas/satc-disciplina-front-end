import { useState } from "react";
import "./App.css";
import ListaCompras, { ItemLista } from "./components/lista-compras/lista-compras";
import FormularioAdicionarItem from "./components/formulario-item/formulario-item";

function App() {
    const [itens, setItens] = useState<Array<ItemLista>>([]);

    const adicionarItem = (novoItem: ItemLista) => {
        setItens([...itens, novoItem]);
    };

    const excluirItem = (index: number) => {
        const novosItens = itens.filter((_item, i) => i !== index);
        setItens(novosItens);
    };

    return (
        <div className="App">
            <header>
                <h2>Lista de Compras:</h2>
            </header>
            <ListaCompras itens={itens} onRemove={excluirItem} />
            <FormularioAdicionarItem onSubmit={adicionarItem} />
        </div>
    );
}

export default App;