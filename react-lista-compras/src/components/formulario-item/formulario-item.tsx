import { useState } from "react";
import { ItemLista } from "../lista-compras/lista-compras";


function FormularioAdicionarItem({ onSubmit }: { onSubmit: Function }) {
    const [novoItem, setNovoItem] = useState<ItemLista>({ descricao: "", quantidade: 0 });
    const [formError, setFormError] = useState<Boolean>(false)

    const submit = (event: any) => {
        event.preventDefault();
        if (!novoItem.descricao.trim()) {
            setFormError(true)
            return
        }

        onSubmit(novoItem);
        setNovoItem({ descricao: "", quantidade: 0 });
        setFormError(false)
    };

    return (
        <form className="form-add-item" onSubmit={submit}>
            <fieldset>
                <div className={`form-group mb-3 ${formError ? 'form-error' : ''}`}>
                    <label htmlFor="nome">Nome do Item <span style={{ color: 'red' }}>*</span></label>
                    <input
                        type="text"
                        className={`form-control`}
                        id="nome"
                        value={novoItem.descricao}
                        onChange={(event) =>
                            setNovoItem({ ...novoItem, descricao: event.target.value })
                        }
                    />
                </div>
                <div className="form-group mb-3">
                    <label htmlFor="quantidade">Quantidade:</label>
                    <input
                        type="number"
                        className="form-control"
                        id="quantidade"
                        value={novoItem.quantidade}
                        onChange={(event) =>
                            setNovoItem({ ...novoItem, quantidade: +event.target.value })
                        }
                    />
                </div>
                <button type="submit" className="btn btn-primary">
                    Adicionar
                </button>
            </fieldset>
        </form>
    );
}

export default FormularioAdicionarItem
