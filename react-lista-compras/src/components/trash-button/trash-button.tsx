import { MouseEventHandler } from "react";

const TrashButton = ({ onClick }: { onClick: MouseEventHandler<HTMLButtonElement> }) => {
	return (
		<button className="btn" style={{ color: 'red' }} onClick={onClick}>
			<i className="bi-trash"></i>
		</button>
	);
};

export default TrashButton;
