import TrashButton from "../trash-button/trash-button";

export interface ItemLista {
    descricao: string,
    quantidade?: number
}

export default function ListaCompras({ itens, onRemove }: { itens: Array<ItemLista>, onRemove: Function }) {
    return (
        <div className="lista-compras-container">
            <table className="table table-striped table-hover align-middle">
                <thead>
                    <tr>
                        <th scope="col" style={{ width: '60%' }}>Descrição</th>
                        <th scope="col" style={{ width: '20%' }} className="text-center">Quantidade</th>
                        <th scope="col" style={{ width: '10' }}></th>
                    </tr>
                </thead>
                <tbody>
                    {itens.map((item: ItemLista, index) => (
                        <tr key={index}>
                            <td>{item.descricao}</td>
                            <td className="text-center">{item.quantidade}</td>
                            <td className="text-center"><TrashButton onClick={() => onRemove(index)} /></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}