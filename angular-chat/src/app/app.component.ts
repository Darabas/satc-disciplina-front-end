import { Component, OnInit } from '@angular/core';

interface IMessage {
    sender: string;
    content: string;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
    ATTENDANT_SENDER = 'Atendente';
    SELF_SENDER = 'Você';

    messages: Array<IMessage> = [];
    inputValue: string = '';

    ngOnInit(): void {
        this.messages.push({
            sender: this.ATTENDANT_SENDER,
            content: 'Lorem ipsum dolor sit amet',
        });

        this.messages.push({
            sender: this.SELF_SENDER,
            content:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        });
    }

    addMessage(): void {
        if (this.inputValue) {
            this.messages.push({
                sender: this.SELF_SENDER,
                content: this.inputValue,
            });

            this.inputValue = '';
        }
    }
}
